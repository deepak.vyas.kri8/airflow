# Import DAG class from airflow package
from airflow import DAG

# Instantiation of smallest DAG with no task and minimum parameter
with DAG(
		dag_id='smallest_dag'
) as dag:
	None